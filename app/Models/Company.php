<?php
    namespace App\Models;

    use illuminate\Database\Eloquent\Model;

    class Company extends Model
    {
        protected $table = 'company';
        protected $fillable = [
            'name'
        ];

        public function users()
        {
            return $this->belongsToMany('App\Models\User');
        }

        public function clients()
        {
            return $this->hasMany('App\Models\Client');
        }





    }

?>
