<?php

    namespace App\Controllers\Company;

    use App\Controllers\Controller;
    use App\Models\User;
    use App\Models\Company;
    use Respect\Validation\Validator as v;

    class CompanyController extends Controller
    {
        public function getCompany($request, $response)
        {
            return $this->view->render($response, 'add\company.twig');
        }

        public function postCompany($request, $response)
        {
            $validation = $this->validator->validate($request, [
                'name' => v::notEmpty()->alpha()
            ]);

            if($validation->failed()){
                return $response->withRedirect($this->router->pathFor('auth.company'));
            }

            $company = Company::create([
                'name'=>$request->getParam('name')
            ]);

            $this->flash->addMessage('info', 'Your company has been created');

            $userId = $_SESSION['user'];
            $company->find($company->id)->users()->attach($userId);

            $_SESSION['company'] = $company->id;

            return $response->withRedirect($this->router->pathFor('home'));

        }
    }
?>
