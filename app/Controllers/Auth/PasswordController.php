<?php

namespace App\Controllers\Auth;

use App\Controllers\Controller;
use App\Models\User;
use Respect\Validation\Validator as v;

class PasswordController extends Controller
{
  public function getChangePassword($rquest, $response)
  {
    $this->view->render($response, 'auth/password/change.twig');
  }

  public function postChangePassword($request, $response)
  {
    $validator = $this->validator->validate($request, [
      'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
      'password' => v::noWhitespace()->notEmpty()
    ]);

    if($validator->failed()){
      return $response->withRedirect($this->router->pathFor('auth.password.change'));
    }

    $this->auth->user()->setPassword($request->getParam('password'));

    $this->flash->addMessage('info', 'Your password was changed.');

    return $response->withREdirect($this->router->pathFor('home'));

  }
};

 ?>
